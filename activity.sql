SELECT customerName FROM customers WHERE country = "Philippines";

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

SELECT firstName, lastName FROM employees WHERE reportsTo IS NULL;

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;S

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

SELECT firstName, lastName, offices.city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode 
	WHERE offices.city = "Tokyo";

SELECT customerName FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber 
	WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

SELECT products.productName, customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode 
	WHERE orders.customerNumber = 121; 

SELECT lastName, firstName, customers.customerName, offices.country FROM employees
	JOIN customers ON customers.salesRepEmployeeNumber = employees.employeeNumber
	JOIN offices ON offices.officeCode = employees.officeCode
	WHERE customers.country = offices.country; 

SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;  

SELECT customerName FROM customers WHERE phone LIKE "+81%";
